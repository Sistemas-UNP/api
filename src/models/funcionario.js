'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({

    nome: {
        type: String,
        required: true,
        trim: true
    },
    cpf: {
        type: String,
        required: true,
        trim: true,
        index: true,
        unique: true
    },
    sexo: {
        type: String,
        required: true
    },
    nomeMae: {
        type: String,
        required: true,
        trim: true
    },
    nomePai: {
        type: String,
        required: true,
        trim: true
    },
    dataNascimento: {
        type: Date,
        required: true
    },
    rg: {
        type: String,
        required: true
    },
    pis: {
        type: String,
        required: true
    },
    titulo: {
        type: String,
        required: true
    },
    banco: {
        type: String,
        required: true
    },
    agencia: {
        type: String,
        required: true
    },
    conta: {
        type: String,
        required: true
    },
    escolaridade: {
        type: String,
        required: true
    },
    estadoCivil: {
        type: String,
        required: true
    },
    matricula: {
        type: Number,
        required: true
    },
    dataAposentadoria: {
        type: Date
    },
    cargo: {
        type: String,
        required: true
    },
    lotacao: {
        type: String,
        required: true
    },
    pne: {
        type: String,
        required: true
    },
    dedicacaoExclusiva: {
        type: Boolean,
        default: false
    },
    cargaHoraria: {
        type: String,
        required: true
    },
    regimeJuridico: {
        type: String,
        required: true
    },
    formaIngresso: {
        type: String,
        required: true
    },
    situacaoFuncional: {
        type: String,
        required: true
    },
    tipoVinculo: {
        type: String,
        required: true
    },
    tipo: {
        type: String,
        required: true
    },
    dataAdmissao: {
        type: Date,
        required: true
    }
});

module.exports = mongoose.model('Funcionario', schema);