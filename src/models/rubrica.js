'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    descricao: {
        type: String,
        required: true,
        trim: true,
        index: true,
        unique: true
    },
    natureza: {
        type: String,
        required: true,
        trim: true
    },
    baselegal: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Rubrica', schema);