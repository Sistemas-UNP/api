'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    remessa: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Remessa'
    },
    servidores: [{
        funcionario: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Funcionario'
        }
    }],
});


module.exports = mongoose.model('QuadroFuncional', schema);