'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    tipo: {
        type: Number,
        required: true
    },
    ano: {
        type: Number,
        required: true 
    },
    mes: {
        type: Number,
        required: true
        
    },
    retificacao: {
        type: Boolean,
        default: false
    },
    datacriacao: {
        type: Date,
        required: true,
        default: Date.now
    },
    status: {
        type: String,       
        default: 'A'
    }
});

module.exports = mongoose.model('Remessa', schema);