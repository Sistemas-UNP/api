'use strict';

const express = require('express');
const router = express.Router();
const controller = require('../controllers/remessa-controller');
const authService = require('../services/auth-service');

router.get('/', controller.get);
router.get('/admin/:id', controller.getById);
router.get('/aberta/', controller.getAberta);
router.post('/', authService.authorize, controller.post);
router.put('/:id', authService.authorize, controller.put);
router.delete('/', authService.authorize, controller.delete);

module.exports = router;