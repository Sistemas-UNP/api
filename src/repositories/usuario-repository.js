'use strict';
const mongoose = require('mongoose');
const Usuario = mongoose.model('Usuario');

exports.create = async(data) => {
    var usuario = new Usuario(data);
    await usuario.save();
}

exports.authenticate = async(data) => {
    const res = await Usuario.findOne({
        email: data.email,
        password: data.password
    });
    return res;
}

exports.getById = async(id) => {
    const res = await Usuario.findById(id);
    return res;
}