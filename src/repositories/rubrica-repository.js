'use strict';
const mongoose = require('mongoose');
const Rubrica = mongoose.model('Rubrica');

exports.get = async() => {
    const res = await Rubrica.find();
    return res;
}

exports.getById = async(id) => {
    const res = await Rubrica
        .findById(id);
    return res;
}

exports.create = async(data) => {
    var rubrica = new Rubrica(data);
    await rubrica.save();
}

exports.update = async(id, data) => {
    await Rubrica
        .findByIdAndUpdate(id, {
            $set: {                
                descricao: data.descricao,
                natureza: data.natureza,
                baselegal: data.baselegal
            }
        });
}

exports.delete = async(id) => {
    await Rubrica
        .findOneAndRemove(id);
}