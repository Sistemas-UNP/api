'use strict';
const mongoose = require('mongoose');
const Funcionario = mongoose.model('Funcionario');

exports.get = async() => {
    const res = await Funcionario.find();
    return res;
}

exports.getById = async(id) => {
    const res = await Funcionario
        .findById(id);
    return res;
}

exports.create = async(data) => {    
    var funcionario = new Funcionario(data);
    await funcionario.save();
}

exports.update = async(id, data) => {
    await Funcionario
        .findByIdAndUpdate(id, {
            $set: {
                nome: data.nome,
                cpf: data.cpf,
                sexo: data.sexo,
                nomeMae: data.nomeMae,
                nomePai: data.nomePai,
                dataNascimento: data.dataNascimento,
                rg: data.rg,
                pis: data.pis,                  
                titulo: data.titulo,
                banco: data.banco,
                agencia: data.agencia,
                conta: data.conta,
                escolaridade: data.escolaridade,
                estadoCivil: data.estadoCivil,
                matricula: data.matricula,
                dataAposentadoria: data.dataAposentadoria,
                cargo: data.cargo,
                lotacao: data.lotacao,
                pne: data.pne,
                dedicacaoExclusiva: data.dedicacaoExclusiva,
                cargaHoraria: data.cargaHoraria,
                regimeJuridico: data.regimeJuridico,
                formaIngresso:data.formaIngresso,
                situacaoFuncional: data.situacaoFuncional,
                tipoVinculo: data.tipoVinculo,
                tipo: data.tipo,
                dataAdmissao: data.dataAdmissao
            }
        });
}

exports.delete = async(id) => {
    await Funcionario
        .findOneAndRemove(id);
}