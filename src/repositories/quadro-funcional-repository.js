'use strict';
const mongoose = require('mongoose');
const QuadroFuncional = mongoose.model('QuadroFuncional');

exports.get = async(data) => {
    var res = await QuadroFuncional.find({})
    .populate('remessa', 'ano mes tipo retificacao')
    .populate('servidores.funcionario', 'nome cpf, tipo');
    return res;
}

exports.create = async(data) => {
    var quadro = new QuadroFuncional(data);
    await quadro.save();
}

exports.update = async(id, data) => {
    await QuadroFuncional
        .findByIdAndUpdate(id, {
            $set: {
                remessa: data.remessa,
                servidores: req.body.servidores
            }
        });
}

exports.delete = async(id) => {
    await QuadroFuncional
        .findOneAndRemove(id);
}