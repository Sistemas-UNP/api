'use strict';
const mongoose = require('mongoose');
const Remessa = mongoose.model('Remessa');

exports.get = async() => {
    const res = await Remessa.find();
    return res;
}

exports.getById = async(id) => {
    const res = await Remessa
        .findById(id);
    return res;
}

exports.getAberta = async() => {
    const res = await Remessa.find({
        status: 'A'
    }, 'tipo ano mes retificacao status');
    return res;
}


exports.create = async(data) => {   
    var remessa = new Remessa(data);
    await remessa.save();
}

exports.update = async(id, data) => {
    await Remessa
        .findByIdAndUpdate(id, {
            $set: {
                tipo: req.body.tipo,
                ano: req.body.ano,
                mes: req.body.mes,
                retificacao: req.body.retificacao,
                datacriacao: req.body.datacriacao,
                status: req.body.status
            }
        });
}

exports.delete = async(id) => {
    await Remessa
        .findOneAndRemove(id);
}