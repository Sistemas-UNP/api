'use strict';

const ValidationContract = require('../validators/fluent-validator');
const repository = require('../repositories/funcionario-repository');
const azure = require('azure-storage');
const guid = require('guid');
var config = require('../config');

exports.get = async(req, res, next) => {
    try {
        var data = await repository.get();
        res.status(200).send(data);
    } catch (e) {
        res.status(500).send({
            message: 'Falha ao processar sua requisição'
        });
    }
}

exports.getById = async(req, res, next) => {
    try {
        var data = await repository.getById(req.params.id);
        res.status(200).send(data);
    } catch (e) {
        res.status(500).send({
            message: 'Falha ao processar sua requisição'
        });
    }
}

exports.post = async(req, res, next) => {
    let contract = new ValidationContract();
    contract.hasMinLen(req.body.nome, 10, 'O nome deve conter pelo menos 10 caracteres');
    contract.isFixedLen(req.body.cpf, 11, 'O CPF deve conter pelo menos 11 caracteres');
    contract.hasMinLen(req.body.nomeMae, 10, 'O nome da mâe conter pelo menos 10 caracteres');
    contract.hasMinLen(req.body.nomePai, 10, 'O nome do pai conter pelo menos 10 caracteres');
    contract.isRequired(req.body.dataNascimento, 'A data de nascimento é obrigatória');
    contract.isRequired(req.body.rg, 'O RG é obrigatório');
    contract.isRequired(req.body.pis, 'O PIS é obrigatório');
    contract.isRequired(req.body.banco, 'O banco é obrigatório');
    contract.isRequired(req.body.agencia, 'A Agência é obrigatória');
    contract.isRequired(req.body.conta,'A conta é obrigatória');
    contract.isRequired(req.body.escolaridade,'A escolaridade é obrigatória');
    contract.isRequired(req.body.estadoCivil,'O estado cívil é obrigatório');
    contract.isRequired(req.body.matricula,'A matrícula é obrigatória');
    contract.isRequired(req.body.cargo,'O cargo é obrigatório');
    contract.isRequired(req.body.lotacao,'A lotação é obrigatória');
    contract.isRequired(req.body.pne,'O PNE é obrigatório');
    contract.isRequired(req.body.dedicacaoExclusiva,'O campo de dedicação não pode ser vazio');
    contract.isRequired(req.body.cargaHoraria,'A carga horária é obrigatória');
    contract.isRequired(req.body.regimeJuridico,'O regime jurídico é obrigatório');
    contract.isRequired(req.body.formaIngresso,'A forma de ingresso obrigatória');
    contract.isRequired(req.body.situacaoFuncional,'A situação funcional é obrigatória');
    contract.isRequired(req.body.tipoVinculo,'O tipo de vínculo é obrigatório');
    contract.isRequired(req.body.tipo,'O tipo de funcionário é obrigatório');
    contract.isRequired(req.body.dataAdmissao,'A data de admissão é obrigatória');
    
    // Se os dados forem inválidos
    if (!contract.isValid()) {
        res.status(400).send(contract.errors()).end();
        return;
    }

    try {
        
        await repository.create({
                nome: req.body.nome,
                cpf: req.body.cpf,
                sexo: req.body.sexo,
                nomeMae: req.body.nomeMae,
                nomePai: req.body.nomePai,
                dataNascimento: req.body.dataNascimento,
                rg: req.body.rg,
                pis: req.body.pis,                  
                titulo: req.body.titulo,
                banco: req.body.banco,
                agencia: req.body.agencia,
                conta: req.body.conta,
                escolaridade: req.body.escolaridade,
                estadoCivil: req.body.estadoCivil,
                matricula: req.body.matricula,
                dataAposentadoria: req.body.dataAposentadoria,
                cargo: req.body.cargo,
                lotacao: req.body.lotacao,
                pne: req.body.pne,
                dedicacaoExclusiva: req.body.dedicacaoExclusiva,
                cargaHoraria: req.body.cargaHoraria,
                regimeJuridico: req.body.regimeJuridico,
                formaIngresso:req.body.formaIngresso,
                situacaoFuncional: req.body.situacaoFuncional,
                tipoVinculo: req.body.tipoVinculo,
                tipo: req.body.tipo,
                dataAdmissao: req.body.dataAdmissao
            });
        res.status(201).send({
            message: 'Funcionário cadastrado com sucesso!'
        });
    } catch (e) {
        console.log(e);
        res.status(500).send({
            message: 'Falha ao processar sua requisição'
        });
    }
};

exports.put = async(req, res, next) => {
    try {
        await repository.update(req.params.id, req.body);
        res.status(200).send({
            message: 'Funcionário atualizado com sucesso!'
        });
    } catch (e) {
        res.status(500).send({
            message: 'Falha ao processar sua requisição'
        });
    }
};

exports.delete = async(req, res, next) => {
    try {
        await repository.delete(req.body.id)
        res.status(200).send({
            message: 'Funcionário removido com sucesso!'
        });
    } catch (e) {
        res.status(500).send({
            message: 'Falha ao processar sua requisição'
        });
    }
};