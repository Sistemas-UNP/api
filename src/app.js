'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const config = require('./config');

const app = express();
const router = express.Router();

// Connecta ao banco
mongoose.connect(config.connectionString);

// Carrega os Models
const Rubrica = require('./models/rubrica');
const Remessa = require('./models/remessa');
const Funcionario = require('./models/funcionario');
const QuadroFunciona = require('./models/quadro-funcional');
const Usuario = require('./models/usuario');


// Carrega as Rotas
const indexRoute = require('./routes/index-route');
const rubricaRoute = require('./routes/rubrica-route');
const remessaRoute = require('./routes/remessa-route');
const funcionarioRoute = require('./routes/funcionario-route');
const quadroFuncionalRoute = require('./routes/quadro-funcional-route');
const usuarioRoute = require('./routes/usuario-route');

app.use(bodyParser.json({
    limit: '5mb'
}));
app.use(bodyParser.urlencoded({
    extended: false
}));

// Habilita o CORS
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, x-access-token');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    next();
});

app.use('/', indexRoute);
app.use('/rubricas', rubricaRoute);
app.use('/remessas', remessaRoute);
app.use('/funcionarios', funcionarioRoute);
app.use('/quadro-funcional', quadroFuncionalRoute);
app.use('/usuarios', usuarioRoute);

module.exports = app;