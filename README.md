# 1972 - Criando APIs com NodeJs

### LINK PARA OS VÍDEOS
Você pode assistir os vídeos deste curso sendo um assinante do site http://balta.io.

http://player.balta.io/#/courses/1972

### Sumário
Neste curso vamos unir a popularidade das APIs com a popularidade do JavaScript criando uma API completa com NodeJs, passando pelos principais pontos que você precisa conhecer para colocar seu projeto em produção.

### Conteúdo Programático
* Instalação Node, NPM e VS Code
* npm init e instalação dos pacotes
* Criando um servidor Web
* Normalizando a porta
* Gerenciando Erros do Servidor
* Iniciando o Debug
* Separando o Servidor
* Configurando o NPM Start
* Nodemon
* CRUD REST
* Rotas
* Controllers
* MongoDb Setup
* Mongooose
* Models
* Criando um Remesa
* Listando as Remessas
* Listando uma Remessa pelo Id
* Atualizando um Remessa
* Excluindo um Remessa
* Validações
* Repositórios
* Async/Await
* Revisitando os Models: Remessa
* Revisitando os Models: Rúbrica
* Revisitando os Models: Funcionários
* Revisitando os Controllers: Remessa
* Revisitando os Controllers: Rúbrica
* Arquivo de Configurações
* Enviando E-mail de Boas Vindas
* Autenticação
* Autorização
* Habilitando CORS
* Publicando a API

